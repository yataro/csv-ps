/**
 * @typedef {import('./csv-parser').CsvParserOptions} CsvParserOptions
 * @typedef {import('./csv-parser').CsvParserCallback} CsvParserCallback
 * @typedef {import('./csv-error').CsvParseError} CsvParseError
 * @typedef {import('./csv-parser').ParsedRow} ParsedRow
 */
/**
 * Valid types for filename.
 * @typedef {string|URL|Buffer} PathLike
 */
/**
 * CSV file parser.
 */
export class CsvFileParser {
    /**
     * Parse CSV file to rows.
     * @async
     * @param {PathLike} filename - Filename of file that will be parsed.
     * @param {import('./csv-parser.js').CsvParserOptions} [options] - Parser options.
     * @param {string} [encoding] - File encoding.
     * @param {number} [buffer_size] - Read buffer size.
     * @returns {Promise.<import('./csv-parser.js').ParsedRow[]>} - Parsed rows.
     */
    static parseAll(filename: PathLike, options?: import('./csv-parser.js').CsvParserOptions, encoding?: string, buffer_size?: number): Promise<import('./csv-parser.js').ParsedRow[]>;
    /**
     * Create a CsvFileParser.
     * @param {import('./csv-parser.js').CsvParserOptions} [parser_options] - Parser options.
     * @param {string} [encoding] - Encoding.
     * @param {number} [buffer_size] - Read buffer size.
     */
    constructor(parser_options?: import('./csv-parser.js').CsvParserOptions, encoding?: string, buffer_size?: number);
    /** @private */
    private _parser;
    /** @private */
    private _string_decoder;
    /** @private */
    private _buffer;
    /**
     * Parse CSV file.
     * @async
     * @param {PathLike} filename - Filename of file that will be parsed.
     * @param {import('./csv-parser.js').CsvParserCallback} cb - Parser callback.
     * @returns {Promise.<undefined>}
     * @throws {import('./csv-error.js').CsvParseError} Invalid CSV file.
     * @throws {Error}
     */
    parse(filename: PathLike, cb: import('./csv-parser.js').CsvParserCallback): Promise<undefined>;
    /**
     * Parse CSV file to rows.
     * @async
     * @param {PathLike} filename - Filename of file that will be parsed.
     * @returns {Promise.<import('./csv-parser.js').ParsedRow[]>} Parsed rows.
     * @throws {import('./csv-error.js').CsvParseError} Invalid CSV file.
     * @throws {Error}
     */
    parseAll(filename: PathLike): Promise<import('./csv-parser.js').ParsedRow[]>;
}

/**
 * Valid types for filename.
 */
export type PathLike = string | URL | Buffer;
