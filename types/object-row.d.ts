export class ObjectRow extends Row {
    constructor(columns: any);
    _row_type: {
        new (fields: any): {
            _fields: any;
            at(i: number): string;
            set(i: number, field: string): void;
            readonly length: number;
            toJSON(): any;
        };
        columns: any;
    };
    row(): {
        _fields: any;
        at(i: number): string;
        set(i: number, field: string): void;
        readonly length: number;
        toJSON(): any;
    };
}
import { Row } from './row.js';
