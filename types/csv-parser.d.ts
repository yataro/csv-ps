/**
 * @typedef {import('./csv-error.js').CsvParseError} CsvParseError
 */
/**
 * CsvParser options.
 * @typedef {Object} CsvParserOptions
 * @property {string} [separator] - CSV field separator.
 * @property {(boolean|string[])} [columns] - Columns setting.
 * @property {boolean} [skip_header] - Whether to skip the header.
 * @property {boolean} [skip_empty_rows] - Whether to skip empty rows.
 * @property {boolean} [trim] - Whether to trim fields while parse.
 */
/**
 * Parsed row.
 * @typedef {(string[]|Row)} ParsedRow
 */
/**
 * @callback CsvParserCallback
 * @param {ParsedRow} row - Parsed row.
 */
/**
 * CSV parser.
 */
export class CsvParser {
    /**
     * Parse CSV string to rows.
     * @param {string} str - String that will be parsed.
     * @param {CsvParserOptions} - Parser options.
     * @returns {ParsedRow[]} Parsed rows.
     */
    static parseAll(str: string, options: any): ParsedRow[];
    /**
     * Default CsvParser options.
     * @type {CsvParserOptions}
     */
    static DefaultOptions: CsvParserOptions;
    /** @private */
    private static _state;
    /**
     * Create a CsvParser.
     * @param {CsvParserOptions} [options] - Parser options.
     */
    constructor(options?: CsvParserOptions);
    /** @private */
    private _line_parser;
    /** @private */
    private _row;
    /** @private */
    private _row_arg;
    /** @private */
    private _separator;
    /** @private */
    private _skip_empty_rows;
    /** @private */
    private _trim;
    /** @private */
    private _cb;
    /** @private */
    private _init;
    /** @private */
    private _state;
    /** @private */
    private _field;
    /** @private */
    private _str;
    /** @private */
    private _char;
    /** @private */
    private _start;
    /** @private */
    private _end;
    /** @private */
    private _pos;
    /** @private */
    private _line;
    /** @private */
    private _line_pos;
    /**
     * Set callback that handles parsed rows.
     * @param {CsvParserCallback} cb - Parser callback.
     */
    setCallback(cb: CsvParserCallback): void;
    /**
     * Parse string.
     * @param {string} str - String that will be parsed.
     * @throws {import('./csv-error.js').CsvParseError} String is invalid.
     */
    parse(str: string): void;
    /**
     * Parse CSV string to rows.
     * @param {string} str - String that will be parsed.
     * @throws {import('./csv-error.js').CsvParseError} String is invalid.
     * @returns {ParsedRow[]} Parsed rows.
     */
    parseAll(str: string): ParsedRow[];
    /**
     * End current row.
     */
    endRow(): void;
    /**
     * Finalize parsing and clear parser state.
     */
    end(): void;
    /**
     * Clear parser state.
     */
    clear(): void;
    /** @private */
    private _flushField;
    /** @private */
    private _setString;
    /** @private */
    private _throwError;
    /** @private */
    private _getField;
    /** @private */
    private _addField;
    /** @private */
    private _jumpStart;
    /** @private */
    private _syncEnd;
    /** @private */
    private _jumpEnd;
    /** @private */
    private _skipChar;
    /** @private */
    private _isWhitespace;
    /** @private */
    private _isIgnored;
    /** @private */
    private _process;
    /** @private */
    private _processSeparator;
    /** @private */
    private _processQuote;
    /** @private */
    private _processLB;
    /** @private */
    private _processChar;
}
/**
 * CsvParser options.
 */
export type CsvParserOptions = {
    /**
     * - CSV field separator.
     */
    separator?: string;
    /**
     * - Columns setting.
     */
    columns?: (boolean | string[]);
    /**
     * - Whether to skip the header.
     */
    skip_header?: boolean;
    /**
     * - Whether to skip empty rows.
     */
    skip_empty_rows?: boolean;
    /**
     * - Whether to trim fields while parse.
     */
    trim?: boolean;
};
/**
 * Parsed row.
 */
export type ParsedRow = (string[] | Row);
export type CsvParserCallback = (row: ParsedRow) => any;
import { Row } from './row-type.js';
