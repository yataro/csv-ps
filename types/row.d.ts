export class Row {
    _fields: any[];
    add(field: any): void;
    end(): boolean;
    row(): any[];
    clear(): void;
    get length(): number;
}
