/**
 * Error that occurs with an invalid CSV.
 */
export class CsvParseError extends Error {
    /**
     * Create a CsvParseError.
     * @param {string} message - Error message.
     * @param {number} line - Line where an error occurred.
     * @param {number} pos - Line position where an error occurred.
     */
    constructor(message: string, line: number, pos: number);
    /**
     * Line where an error occurred.
     * @type {number}
     * @public
     */
    public line: number;
    /**
     * Line position where an error occurred.
     * @type {number}
     * @public
     */
    public pos: number;
}
