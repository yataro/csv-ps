export function createRowType(columns: any): {
    new (fields: any): {
        /** @private */
        _fields: any;
        /**
         * Get field by index.
         * @param {number} i - Field index.
         * @returns {string} Field.
         */
        at(i: number): string;
        /**
         * Set field by index.
         * @param {number} i - Field index.
         * @param {string} field - Field.
         */
        set(i: number, field: string): void;
        /**
         * Get field buffer length.
         * @type {number}
         */
        readonly length: number;
        /**
         * Convert row to regular JSON object.
         * @returns {Object} Row object.
         */
        toJSON(): any;
    };
    columns: any;
};
/**
 * Represents row with named columns.
 */
export class Row {
    /**
     * Row columns.
     * @type {string[]}
     * @static
     */
    static columns: string[];
    /**
     * @hideconstructor
     * @package
     */
    constructor(fields: any);
    /** @private */
    private _fields;
    /**
     * Get field by index.
     * @param {number} i - Field index.
     * @returns {string} Field.
     */
    at(i: number): string;
    /**
     * Set field by index.
     * @param {number} i - Field index.
     * @param {string} field - Field.
     */
    set(i: number, field: string): void;
    /**
     * Get field buffer length.
     * @type {number}
     */
    get length(): number;
    /**
     * Convert row to regular JSON object.
     * @returns {Object} Row object.
     */
    toJSON(): any;
}
