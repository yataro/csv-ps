/**
 * @typedef {import('./csv-parser.js').ParsedRow} ParsedRow
 */
/**
 * CsvSerializerParser options.
 * @typedef {Object} CsvSerializerOptions
 * @property {string} [separator] - CSV field separator.
 * @property {boolean} [quote_numbers] - Whether to quote numbers.
 * @property {boolean} [crlf] - Whether to use <CR><LF> line break.
 */
/**
 * Serializable row.
 * @typedef {(string[]|ParsedRow)} RowLike
 */
/**
 * CSV serializer.
 */
export class CsvSerializer {
    /**
     * Serialize rows.
     * @param {RowLike[]} rows - Rows that will be serialized.
     * @param {CsvSerializerOptions} [options] - Serializer options.
     * @returns {string} Serialized rows.
     */
    static serializeAll(rows: RowLike[], options?: CsvSerializerOptions): string;
    /** @private */
    private static _quoteString;
    /** @private */
    private static _escapeString;
    /**
     * Default CsvSerializer options.
     * @type {CsvSerializerOptions}
     */
    static DefaultOptions: CsvSerializerOptions;
    /**
     * Create a CsvSerializer.
     * @param {CsvSerializerOptions} [options] - Serializer options.
     */
    constructor(options?: CsvSerializerOptions);
    /** @private */
    private _separator;
    /** @private */
    private _quote_numbers;
    /** @private */
    private _line_break;
    /**
     * Serialize row.
     * @param {RowLike} row - Row that will be serialized.
     * @returns {string} Serialized row.
     */
    serializeRow(row: RowLike): string;
    /**
     * Serialize rows.
     * @param {RowLike[]} rows - Rows that will be serialized.
     * @returns {string} Serialized rows.
     */
    serializeAll(rows: RowLike[]): string;
    /** @private */
    private _serializeField;
}
/**
 * CsvSerializerParser options.
 */
export type CsvSerializerOptions = {
    /**
     * - CSV field separator.
     */
    separator?: string;
    /**
     * - Whether to quote numbers.
     */
    quote_numbers?: boolean;
    /**
     * - Whether to use <CR><LF> line break.
     */
    crlf?: boolean;
};
/**
 * Serializable row.
 */
export type RowLike = (string[] | import('./csv-parser.js').ParsedRow);
