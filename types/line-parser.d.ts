export class LineParser {
    set(str: any): any;
    _pos: any;
    _cr: boolean;
    _end: any;
    _str: any;
    next(): any;
    CR(): boolean;
    pos(): any;
    end(): any;
    clear(): void;
}
