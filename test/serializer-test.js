import {describe, it} from 'node:test';
import assert from 'node:assert';
import {CsvSerializer} from '../lib/index.js';

function assertSerializedEqual(rows, serialized, options = undefined) {
	const serialized_rows = CsvSerializer.serializeAll(rows, options);

	assert.equal(serialized_rows, serialized);
}

describe('CSV serializer', () => {
	it('should serialize', () => {
		assertSerializedEqual([['a', 'b', 'c']], '"a","b","c"\n');
	});

	it('should serialize null and undefined as empty fields', () => {
		assertSerializedEqual([[null, undefined, '']], ',,""\n');
	});

	it('should serialize with custom separator', () => {
		assertSerializedEqual([['a', 'b', 'c']], '"a";"b";"c"\n', {
			separator: ';'
		});
	});


	it('should serialize with <CR><LF> line break', () => {
		assertSerializedEqual([['a', 'b', 'c']], '"a","b","c"\r\n', {
			crlf: true
		});
	});

	it('should escape quotes', () => {
		assertSerializedEqual([['a"b', '"c"', '"']], '"a""b","""c""",""""\n');
	});

	it('should not quote numbers', () => {
		assertSerializedEqual([[1, 2, 3]], '1,2,3\n');
	});

	it('should quote numbers', () => {
		assertSerializedEqual([[1, 2, 3]], '"1","2","3"\n', {
			quote_numbers: true
		});
	});
});
