import {describe, it} from 'node:test';
import assert from 'node:assert';
import {CsvParser, CsvParseError} from '../lib/index.js';

function assertRowsEqual(csv, rows, options = undefined) {
	const parsed_rows = CsvParser.parseAll(csv, options);

	assert.deepEqual(parsed_rows, rows);
}

function assertObjectRowsEqual(csv, rows, options = undefined) {
	const parsed_rows = CsvParser.parseAll(csv, options).map((row) => row.toJSON());

	assert.deepEqual(parsed_rows, rows);
}

describe('CSV parser', () => {
	it('should parse', () => {
		assertRowsEqual('a,b,c\nd,e,f', [['a', 'b', 'c'], ['d', 'e', 'f']]);
	});

	it('should parse with custom separator', () => {
		assertRowsEqual('a;b;c\nd;e;f', [['a', 'b', 'c'], ['d', 'e', 'f']], {
			separator: ';'
		});
	});

	it('should parse with empty fields', () => {
		assertRowsEqual('a,,b\n,c,\n,,', [['a', null, 'b'], [null, 'c', null], [null, null, null]]);
	});

	it('should parse with <CR><LF> line break', () => {
		assertRowsEqual('a,b,c\r\nd,e,f', [['a', 'b', 'c'], ['d', 'e', 'f']]);
	});

	it('should parse with quoted fields', () => {
		assertRowsEqual('"a","b","c"\n"d","e","f"', [['a', 'b', 'c'], ['d', 'e', 'f']]);
	});

	it('should parse with empty quoted fields', () => {
		assertRowsEqual('a,"",b\n"",c,""\n"","",""', [['a', null, 'b'], [null, 'c', null], [null, null, null]]);
	});

	it('should parse with quoted line breaks', () => {
		assertRowsEqual(
			'"\r\n","a\r\nb","\r\nc","d\r\n"\r\n"\n","e\nf","\ng","h\n"',
			[['\r\n', 'a\r\nb', '\r\nc', 'd\r\n'], ['\n', 'e\nf', '\ng', 'h\n']]
		);
	});

	it('should parse with quoted commas', () => {
		assertRowsEqual('"a,b",",c,",","', [['a,b', ',c,', ',']]);
	});

	it('should use header', () => {
		assertObjectRowsEqual(
			'a,b,c\n1,2,3\n4,5,6',
			[{a: '1', b: '2', c: '3'}, {a: '4', b: '5', c: '6'}],
			{columns: true}
		);
	});

	it('should use provided columns', () => {
		assertObjectRowsEqual('1,2,3\n4,5,6', [{a: '1', b: '2', c: '3'}, {a: '4', b: '5', c: '6'}], {
			columns: ['a', 'b', 'c']
		});
	});

	it('should skip header', () => {
		assertRowsEqual('a,b,c\n1,2,3\n4,5,6', [['1', '2', '3'], ['4', '5', '6']], {
			skip_header: true
		});
	});

	it('should skip empty rows', () => {
		assertRowsEqual('\n\na,b,c\n\n\n\r\n,', [['a', 'b', 'c'], [null, null]]);
	});

	it('should keep empty rows', () => {
		assertRowsEqual(
			'\n\na,b,c\n\n\n\r\n',
			[[null], [null], ['a', 'b', 'c'], [null], [null], [null], [null]],
			{skip_empty_rows: false}
		);
	});

	it('should trim fields', () => {
		assertRowsEqual(
			'  a  ,\t\tb\t\t,\r\rc\r\r\n  "d"  ,\t\t"e"\t\t,\r\r"f"\r\r',
			[['a', 'b', 'c'], ['d', 'e', 'f']],
			{trim: true}
		);
	})

	it('should fail to parse', () => {
		assert.throws(() => {
			CsvParser.parseAll('  "a"  ,\t\t"b"\t\t,\r\r"c"\r\r');
		}, CsvParseError);
	});

	it('should escape quotes', () => {
		assertRowsEqual('"a""b","c""","""""",""', [['a"b', 'c"', '""', null]]);
	});

	it('should parse csv char by char', () => {
		const csv = `a,b,c\r\nd,e,f\n"g","h","i"\r\n`;
		const rows = [];

		const parser = new CsvParser();
		parser.setCallback(rows.push.bind(rows));

		for (const char of csv) {
			parser.parse(char);
		}
		parser.end();

		assert.deepEqual(rows, [['a', 'b', 'c'], ['d', 'e', 'f'], ['g', 'h', 'i']]);
	});
});
