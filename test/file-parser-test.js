import {describe} from 'node:test';
import assert from 'node:assert';
import {CsvFileParser} from '../lib/index.js';

describe('CSV file parser', async () => {
	const rows = (await CsvFileParser.parseAll(new URL('test.csv', import.meta.url), {
		columns: true
	})).map((row) => row.toJSON());

	assert.deepEqual(rows, [{a: '1', b: '2', c: '3'}, {a: '4', b: '5', c: '6'}]);
});
