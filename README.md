
# csv-ps

Simple but reliable CSV parser/serializer.

## Features

* Simple and minimal.
* RFC 4180 compliant.
* Only few but necessary options.
* Can be easily wrapped to other API's.
* No deps.

## Examples

#### Parse string:
```js
import {CsvParser} from 'csv-ps';

const parser = new CsvParser();
const rows = parser.parseAll('1,2,3');

// rows: [['1', '2', '3']]
```

#### Parse by chunk:
```js
import {CsvParser} from 'csv-ps';

const parser = new CsvParser();

const rows = [];

parser.setCallback((row) => {
    rows.push(row);
});

parser.parse('1');
parser.parse(',2,');
parser.parse('3\n');
parser.parse('4,5');
parser.parse(',6');

parser.end();

// rows: [['1', '2', '3'], ['4', '5', '6']]
```

#### Parse file:
```js
import {CsvFileParser} from 'csv-ps';

const parser = CsvFileParser();

await parser.parse('/path/to/file.csv', (row) => {
    console.log('Got a row: %O', row);
});
```

#### Serialize rows:
```js
import {CsvSerializer} from 'csv-ps';

const serializer = new CsvSerializer();

const serialized = serializer.serializeAll([['a', 'b', 'c']]);

// serialized: '"a","b","c"\n'
```

## Author
[Aikawa Yataro](https://gitlab.com/yataro)
