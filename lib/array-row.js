import {Row} from './row.js';

export class ArrayRow extends Row {
	constructor(skip_header) {
		super();

		this._skip_header = skip_header;
	}

	end() {
		if (!super.end()) return false;

		if (this._skip_header) {
			this._skip_header = false;

			this.clear();

			return false;
		}

		return true;
	}
};
