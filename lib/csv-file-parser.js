import fs from 'node:fs/promises';
import {StringDecoder} from 'node:string_decoder';
import {CsvParser} from './csv-parser.js';

/**
 * @typedef {import('./csv-parser.js').CsvParserOptions} CsvParserOptions
 * @typedef {import('./csv-parser.js').CsvParserCallback} CsvParserCallback
 * @typedef {import('./csv-error.js').CsvParseError} CsvParseError
 * @typedef {import('./csv-parser.js').ParsedRow} ParsedRow
 */

/**
 * Valid types for filename.
 * @typedef {string|URL|Buffer} PathLike
 */

/**
 * CSV file parser.
 */
export class CsvFileParser {
	/**
	 * Create a CsvFileParser.
	 * @param {CsvParserOptions} [parser_options] - Parser options.
	 * @param {string} [encoding] - Encoding.
	 * @param {number} [buffer_size] - Read buffer size.
	 */
	constructor(parser_options  = undefined, encoding = 'utf8', buffer_size = 8192) {
		/** @private */
		this._parser = new CsvParser(parser_options);
		/** @private */
		this._string_decoder = new StringDecoder(encoding);
		/** @private */
		this._buffer = Buffer.alloc(buffer_size);
	}

	/**
	 * Parse CSV file.
	 * @async
	 * @param {PathLike} filename - Filename of file that will be parsed.
	 * @param {CsvParserCallback} cb - Parser callback.
	 * @returns {Promise.<undefined>}
	 * @throws {CsvParseError} Invalid CSV file.
	 * @throws {Error}
	 */
	async parse(filename, cb) {
		this._parser.setCallback(cb);

		const file = await fs.open(filename);

		try {
			let bytes_read;

			do {
				bytes_read = (await file.read(this._buffer, 0, this._buffer.length)).bytesRead;

				const string = this._string_decoder.write(this._buffer.subarray(0, bytes_read));

				this._parser.parse(string);

			} while (bytes_read === this._buffer.length);

			this._parser.parse(this._string_decoder.end());
			this._parser.end();
		} finally {
			file.close();
		}
	}

	/**
	 * Parse CSV file to rows.
	 * @async
	 * @param {PathLike} filename - Filename of file that will be parsed.
	 * @returns {Promise.<ParsedRow[]>} Parsed rows.
	 * @throws {CsvParseError} Invalid CSV file.
	 * @throws {Error}
	 */
	async parseAll(filename) {
		const rows = [];

		await this.parse(filename, rows.push.bind(rows));

		return rows;
	}

	/**
	 * Parse CSV file to rows.
	 * @async
	 * @param {PathLike} filename - Filename of file that will be parsed.
	 * @param {CsvParserOptions} [options] - Parser options.
	 * @param {string} [encoding] - File encoding.
	 * @param {number} [buffer_size] - Read buffer size.
	 * @returns {Promise.<ParsedRow[]>} - Parsed rows.
	 */
	static parseAll(filename, options = undefined, encoding = 'utf8', buffer_size = 8192) {
		const parser = new CsvFileParser(options, encoding, buffer_size);

		return parser.parseAll(filename);
	}
};
