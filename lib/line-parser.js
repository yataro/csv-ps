// RFC 4180 requires <CR><LF> line breaks, but this implementation also tolerant to <LF> and a mix of both.
// <CR> which does't belongs to line breaks, remains untouched.

export class LineParser {
	constructor() {
		this.clear();
	}

	set(str) {
		this._pos = 0;

		if (this._cr) {
			this._cr = false;

			str = '\r' + str;
		}

		this._end = str.length;

		if (str.endsWith('\r')) {
			this._cr = true;
			this._end--;
		}

		this._str = str;

		return str;
	}

	next() {
		let br = this._str.indexOf('\n', this._pos);

		this._pos = br + 1;

		if (br === -1 || br === 0) return br;

		if (this._str[br - 1] === '\r') {
			br--;
		}

		return br;
	}

	CR() {
		return this._cr;
	}

	pos() {
		return this._pos;
	}

	end() {
		return this._end;
	}

	clear() {
		this._cr = false;
		this._pos = -1;
		this._end = -1;
		this._str = undefined;
	}
};
