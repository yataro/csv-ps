/**
 * @typedef {import('./csv-parser.js').ParsedRow} ParsedRow
 */

/**
 * CsvSerializerParser options.
 * @typedef {Object} CsvSerializerOptions
 * @property {string} [separator] - CSV field separator.
 * @property {boolean} [quote_numbers] - Whether to quote numbers.
 * @property {boolean} [crlf] - Whether to use <CR><LF> line break.
 */

/**
 * Serializable row.
 * @typedef {(string[]|ParsedRow)} RowLike
 */

/**
 * CSV serializer.
 */
export class CsvSerializer {
	/**
	 * Create a CsvSerializer.
	 * @param {CsvSerializerOptions} [options] - Serializer options.
	 */
	constructor(options = undefined) {
		options = {...CsvSerializer.DefaultOptions, ...options};

		if (options.separator.length !== 1) {
			throw new RangeError('Separator length must be equal to 1.');
		}

		/** @private */
		this._separator = options.separator;
		/** @private */
		this._quote_numbers = Boolean(options.quote_numbers);
		/** @private */
		this._line_break = (options.crlf) ? '\r\n' : '\n';
	}

	/**
	 * Serialize row.
	 * @param {RowLike} row - Row that will be serialized.
	 * @returns {string} Serialized row.
	 */
	serializeRow(row) {
		if (row.length === 0) return '';

		let result = this._serializeField(row.at(0));

		for (let i = 1; i < row.length; i++) {
			const field = row.at(i);

			result += this._separator + this._serializeField(field);
		}

		return result + this._line_break;
	}

	/**
	 * Serialize rows.
	 * @param {RowLike[]} rows - Rows that will be serialized.
	 * @returns {string} Serialized rows.
	 */
	serializeAll(rows) {
		let result = '';

		for (let i = 0; i < rows.length; i++) {
			result += this.serializeRow(rows[i]);
		}

		return result;
	}

	/**
	 * Serialize rows.
	 * @param {RowLike[]} rows - Rows that will be serialized.
	 * @param {CsvSerializerOptions} [options] - Serializer options.
	 * @returns {string} Serialized rows.
	 */
	static serializeAll(rows, options = undefined) {
		const serializer = new CsvSerializer(options);

		return serializer.serializeAll(rows);
	}

	/** @private */
	_serializeField(field) {
		if (field === null || field === undefined) {
			return '';
		}

		const proto = Object.getPrototypeOf(field);

		if (proto === String.prototype) {
			field = CsvSerializer._quoteString(CsvSerializer._escapeString(field));
		} else if (proto === Number.prototype) {
			field = field.toString();

			if (this._quote_numbers) {
				field = CsvSerializer._quoteString(field);
			}
		} else {
			field = field.toString();
		}

		return field;
	}

	/** @private */
	static _quoteString(str) {
		return '"' + str + '"';
	}

	/** @private */
	static _escapeString(str) {
		return str.replaceAll('"', '""');
	}

	/**
	 * Default CsvSerializer options.
	 * @type {CsvSerializerOptions}
	 */
	static DefaultOptions = {
		separator: ',',
		quote_numbers: false,
		crlf: false
	};
};
