export class Row {
	constructor() {
		this._fields = [];
	}

	add(field) {
		this._fields.push(field);
	}

	end() {
		return this.length !== 0;
	}

	row() {
		const row = this._fields;

		this.clear();

		return row;
	}

	clear() {
		this._fields = []; // Not to destroy the previous instance.
	}

	get length() {
		return this._fields.length;
	}
};
