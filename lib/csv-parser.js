import {LineParser} from './line-parser.js';
import {ArrayRow} from './array-row.js'
import {ObjectRow} from './object-row.js';
import {CsvParseError} from './csv-error.js';
import {Row} from './row-type.js';

/**
 * @typedef {import('./csv-error.js').CsvParseError} CsvParseError
 */

/**
 * CsvParser options.
 * @typedef {Object} CsvParserOptions
 * @property {string} [separator] - CSV field separator.
 * @property {(boolean|string[])} [columns] - Columns setting.
 * @property {boolean} [skip_header] - Whether to skip the header.
 * @property {boolean} [skip_empty_rows] - Whether to skip empty rows.
 * @property {boolean} [trim] - Whether to trim fields while parse.
 */

/**
 * Parsed row.
 * @typedef {(string[]|Row)} ParsedRow
 */

/**
 * @callback CsvParserCallback
 * @param {ParsedRow} row - Parsed row.
 */

/**
 * CSV parser.
 */
export class CsvParser {
	/**
	 * Create a CsvParser.
	 * @param {CsvParserOptions} [options] - Parser options.
	 */
	constructor(options = undefined) {
		options = {...CsvParser.DefaultOptions, ...options};

		if (options.separator.length !== 1) {
			throw new RangeError('Separator length must be equal to 1.');
		}

		/** @private */
		this._line_parser = new LineParser();

		/** @private */
		this._row = undefined;
		/** @private */
		this._row_arg = undefined;

		if (options.columns === true) {
			this._row = new ObjectRow();
		} else if (options.columns instanceof Array) {
			this._row = new ObjectRow(options.columns);

			this._row_arg = options.columns;
		} else {
			const skip_header = Boolean(options.skip_header);

			this._row = new ArrayRow(skip_header);

			this._row_arg = skip_header;
		}

		/** @private */
		this._separator = options.separator;
		/** @private */
		this._skip_empty_rows = Boolean(options.skip_empty_rows);
		/** @private */
		this._trim = Boolean(options.trim);

		/** @private */
		this._cb = undefined;

		this._init();
	}

	/** @private */
	_init() {
		/** @private */
		this._state = CsvParser._state.NONE;

		/** @private */
		this._field = null;
		/** @private */
		this._str = undefined;
		/** @private */
		this._char = undefined;

		/** @private */
		this._start = -1;
		/** @private */
		this._end = -1;
		/** @private */
		this._pos = -1;

		/** @private */
		this._line = 1; // Lines starts at 1.
		/** @private */
		this._line_pos = 0;
	}

	/**
	 * Set callback that handles parsed rows.
	 * @param {CsvParserCallback} cb - Parser callback.
	 */
	setCallback(cb) {
		this._cb = cb;
	}

	/**
	 * Parse string.
	 * @param {string} str - String that will be parsed.
	 * @throws {CsvParseError} String is invalid.
	 */
	parse(str) {
		this._setString(str);

		let br = this._line_parser.next();

		const end = this._line_parser.end();

		for (; this._pos < end; this._pos++) {
			if (this._pos == br) {
				this._processLB();

				br = this._line_parser.next();

				continue;
			}

			this._char = this._str[this._pos];

			this._process();

			this._line_pos++;
		}
	}

	/**
	 * Parse CSV string to rows.
	 * @param {string} str - String that will be parsed.
	 * @throws {CsvParseError} String is invalid.
	 * @returns {ParsedRow[]} Parsed rows.
	 */
	parseAll(str) {
		const rows = [];

		this.setCallback(rows.push.bind(rows));
		this.parse(str);
		this.end();

		return rows;
	}

	/**
	 * End current row.
	 */
	endRow() {
		if (this._skip_empty_rows && (this._row.length === 0 && (this._start === this._end && this._field === null))) {
			return; // Skip empty row.
		}

		this._addField();

		if (this._row.end()) {
			this._cb(this._row.row());
		}
	}

	/**
	 * Finalize parsing and clear parser state.
	 */
	end() {
		if (this._line_parser.CR()) { // Check for unprocessed <CR>.
			this._char = '\r';

			this._processChar();
		}

		switch(this._state) {
			case CsvParser._state.QUOTED_START:
			case CsvParser._state.QUOTED:
				this._throwError('Unexpected end of quoted string.');
		}

		this.endRow();

		this.clear();
	}

	/**
	 * Clear parser state.
	 */
	clear() {
		this._line_parser.clear();
		this._row = new this._row.constructor(this._row_arg);

		this._init();
	}

	/** @private */
	_flushField() {
		this._field = this._getField();
	}

	/** @private */
	_setString(str) {
		this._flushField();

		this._str = this._line_parser.set(str);

		this._pos = 0;
		this._start = 0;
		this._end = 0;
	}

	/** @private */
	_throwError(message) {
		throw new CsvParseError(message, this._line, this._line_pos);
	}

	/** @private */
	_getField() {
		let field;

		if (this._start === this._end) {
			field = this._field;

			this._field = null;
		} else {
			field = this._str.substring(this._start, this._end);

			if (this._field !== null) {
				field = this._field + field;

				this._field = null;
			}
		}

		return field;
	}

	/** @private */
	_addField() {
		const field = this._getField();

		this._row.add(field);

		this._state = CsvParser._state.NONE;
	}

	/** @private */
	_jumpStart() {
		this._start = this._pos + 1;
	}

	/** @private */
	_syncEnd() {
		this._end = this._pos;
	}

	/** @private */
	_jumpEnd() {
		this._end = this._pos + 1;
	}

	/** @private */
	_skipChar() {
		this._syncEnd();
		this._flushField();
		this._jumpStart();
	}

	/** @private */
	_isWhitespace() {
		const char_code = this._char.codePointAt();

		return	(char_code >= 9 && char_code <= 13) ||
				char_code === 32 || char_code === 133 || char_code === 160 ||
				char_code === 5760 || (char_code >= 8192 && char_code <= 8202) ||
				char_code === 8232 || char_code === 8233 || char_code === 8239 ||
				char_code === 8287 || char_code === 12288;
	}

	/** @private */
	_isIgnored() {
		return this._trim && this._isWhitespace();
	}

	/** @private */
	_process() {
		switch (this._char) {
			case this._separator:
				this._processSeparator();

				break;
			case '"':
				this._processQuote();

				break;
			default:
				this._processChar();

				break;
		}
	}

	/** @private */
	_processSeparator() {
		switch (this._state) {
			case CsvParser._state.UNQUOTED:
			case CsvParser._state.NONE:
			case CsvParser._state.QUOTED_END:
				this._addField();
				this._jumpStart();

				break;
			case CsvParser._state.QUOTED_START:
				this._state = CsvParser._state.QUOTED;
		}

		this._jumpEnd();
	}

	/** @private */
	_processQuote() {
		switch (this._state) {
			case CsvParser._state.UNQUOTED:
				this._throwError('Unexpected quote in unquoted field.');

				break;
			case CsvParser._state.QUOTED:
				this._state = CsvParser._state.QUOTED_END;

				this._syncEnd();

				break;

			case CsvParser._state.NONE:
				this._state = CsvParser._state.QUOTED_START;

				this._jumpStart();
				this._jumpEnd();

				break;
			case CsvParser._state.QUOTED_START:
				this._state = CsvParser._state.QUOTED_END;

				break;
			case CsvParser._state.QUOTED_END:
				this._state = CsvParser._state.QUOTED;

				this._skipChar();
		}
	}

	/** @private */
	_processLB() {
		switch (this._state) {
			case CsvParser._state.UNQUOTED:
			case CsvParser._state.NONE:
			case CsvParser._state.QUOTED_END:
				this.endRow();

				this._start = this._line_parser.pos();
				this._end = this._start;
				this._pos = this._start - 1;

				break;
			case CsvParser._state.QUOTED:
				break;
			case CsvParser._state.QUOTED_START:
				this._state = CsvParser._state.QUOTED;

				this._jumpEnd();
		}

		this._line++;
		this._line_pos = 0;
	}

	/** @private */
	_processChar() {
		switch (this._state) {
			case CsvParser._state.UNQUOTED:
				if (this._isIgnored()) return;

				break;
			case CsvParser._state.NONE:
				if (this._isIgnored()) {
					this._jumpStart();

					return;
				}

				this._state = CsvParser._state.UNQUOTED;
				break;
			case CsvParser._state.QUOTED:
				break;
			case CsvParser._state.QUOTED_START:
				this._state = CsvParser._state.QUOTED;

				break;
			case CsvParser._state.QUOTED_END:
				if (this._isIgnored()) return;

				this._throwError('Unexpected char at end of quoted field.');
		}

		this._jumpEnd();
	}

	/**
	 * Parse CSV string to rows.
	 * @param {string} str - String that will be parsed.
	 * @param {CsvParserOptions} - Parser options.
	 * @returns {ParsedRow[]} Parsed rows.
	 */
	static parseAll(str, options) {
		const parser = new CsvParser(options);

		return parser.parseAll(str);
	}

	/**
	 * Default CsvParser options.
	 * @type {CsvParserOptions}
	 */
	static DefaultOptions = {
		separator: ',',
		columns: false,
		skip_header: false,
		skip_empty_rows: true,
		trim: false
	};

	/** @private */
	static _state = {
		NONE:			0,
		UNQUOTED:		1,
		QUOTED:			2,
		QUOTED_START:	3,
		QUOTED_END:		4
	};
};
