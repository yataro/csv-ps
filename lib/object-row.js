import {Row} from './row.js';
import {createRowType} from './row-type.js';

export class ObjectRow extends Row {
	constructor(columns) {
		super();

		this._row_type = (columns === undefined) ? undefined : createRowType(columns);
	}

	end() {
		if (!super.end()) return false;

		if (this._row_type === undefined) {
			this._row_type = createRowType(this._fields);

			this.clear();

			return false;
		}

		return true;
	}

	row() {
		const row = super.row();

		if (row.length < this._row_type.columns.length) {
			const start = row.length;
			row.length = this._row_type.columns.length;

			row.fill(null, start);
		}

		return new this._row_type(row);
	}
};
