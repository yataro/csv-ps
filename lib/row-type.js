/**
 * Represents row with named columns.
 */
export class Row {
	/**
	 * @hideconstructor
	 * @package
	 */
	constructor(fields) {
		/** @private */
		this._fields = fields;
	}

	/**
	 * Get field by index.
	 * @param {number} i - Field index.
	 * @returns {string} Field.
	 */
	at(i) {
		return this._fields[i];
	}

	/**
	 * Set field by index.
	 * @param {number} i - Field index.
	 * @param {string} field - Field.
	 */
	set(i, field) {
		this._fields[i] = field;
	}

	/**
	 * Get field buffer length.
	 * @type {number}
	 */
	get length() {
		return this._fields.length;
	}

	/**
	 * Convert row to regular JSON object.
	 * @returns {Object} Row object.
	 */
	toJSON() {
		const obj = {};

		for (let i = 0; i < this.constructor.columns.length; i++) {
			const column = this.constructor.columns[i];

			if (this.constructor.columns.indexOf(column) !== i) {
				// Duplicate column.
				continue;
			}

			obj[column] = this._fields[i];
		}

		return obj;
	}

	/**
	 * Row columns.
	 * @type {string[]}
	 * @static
	 */
	static columns = [];
};

function columnGetter(i) {
	return this._fields[i];
}

function columnSetter(i, value) {
	return this._fields[i] = value;
}

export function createRowType(columns) {
	class RowType extends Row {};

	RowType.columns = columns;

	for (let i = 0; i < columns.length; i++) {
		const column = columns[i];

		if (columns.indexOf(column) !== i) {
			// Duplicate column.
			continue;
		}

		Object.defineProperty(RowType.prototype, column, {
			get: function() {
				return columnGetter.call(this, i);
			},
			set: function() {
				return columnSetter.call(this, i);
			}
		});
	}

	return RowType;
}
