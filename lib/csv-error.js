/**
 * Error that occurs with an invalid CSV.
 */
export class CsvParseError extends Error {
	/**
	 * Create a CsvParseError.
	 * @param {string} message - Error message.
	 * @param {number} line - Line where an error occurred.
	 * @param {number} pos - Line position where an error occurred.
	 */
	constructor(message, line, pos) {
		super(message);

		/**
		 * Line where an error occurred.
		 * @type {number}
		 * @public
		 */
		this.line = line;
		/**
		 * Line position where an error occurred.
		 * @type {number}
		 * @public
		 */
		this.pos = pos;
	}
};
